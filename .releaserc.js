module.exports = {
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: [
                'CHANGELOG.md',
                "Cargo.toml",
                "Cargo.lock",
                "docker-seq/Cargo.toml",
                "docker-seq/Cargo.lock",
                "docker-seq-lib/Cargo.toml",
                "docker-seq-lib/Cargo.lock",
                "docker-seq-c-lib/Cargo.toml",
                "docker-seq-c-lib/Cargo.lock",
            ],
        }],
        ['@semantic-release/gitlab', {
            assets: [
                {
                    label: `docker-seq-\${nextRelease.version}-x86_64-unknown-linux-musl`,
                    path: `target/x86_64-unknown-linux-musl/release/docker-seq`,
                },
                {
                    label: `dockerseq.h-\${nextRelease.version}-x86_64-unknown-linux-musl`,
                    path: `target/x86_64-unknown-linux-musl/release/dockerseq.h`,
                },
                {
                    label: `dockerseq.hpp-\${nextRelease.version}-x86_64-unknown-linux-musl`,
                    path: `target/x86_64-unknown-linux-musl/release/dockerseq.hpp`,
                },
                {
                    label: `dockerseq.pxd-\${nextRelease.version}-x86_64-unknown-linux-musl`,
                    path: `target/x86_64-unknown-linux-musl/release/dockerseq.pxd`,
                },
                {
                    label: `libdockerseq.a-\${nextRelease.version}-x86_64-unknown-linux-musl`,
                    path: `target/x86_64-unknown-linux-musl/release/libdockerseq.a`,
                },
            ],
        }]
    ],
};
